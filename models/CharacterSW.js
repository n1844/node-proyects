const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const schemaSW = new Schema({

    name: { type: String, required: true },
    power: { type: Number, default: 0 },
    side: { type: String, default: "XD" },
    type: { type: String, required: true },
    job: { type: String, required: true },
}, {
    timestamps: true
});

const CharactersSW = mongoose.model('charactersws', schemaSW);
module.exports = CharactersSW;