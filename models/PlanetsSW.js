const mongoose = require("mongoose");
const CharactersSW = require("./CharacterSW");

const Schema = mongoose.Schema;

const planetsSchema = new Schema({
    name: { type: String, required: true },
    squad: { type: String, required: true, default: "Neutral" },
    // ref :"charactersws" debe coincider exactamente con la coleccion del MODELO al que se hace referencia
    characters: [{ type: mongoose.Types.ObjectId, ref: 'charactersws' }]

}, { timestamps: true });

const PlanetsSW = mongoose.model("planetssw", planetsSchema);
module.exports = PlanetsSW