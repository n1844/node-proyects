// const { Router } = require('express');
const express = require('express');
const router = express.Router();
//Molde de Personajes
const CharactersSW = require('./models/CharacterSW');
//Rutas de personajes
const characterRouter = require('./routes/characters.routes');
//Routes Planets
const planetsRouter = require("./routes/planets.routes")
    //Rutas de HOME
const indexRouter = require('./routes/index.routes');
//Conectcion con la DB
const { connectToDb } = require('./config/db');
connectToDb();

const PORT = 3000;
const server = express();

server.use(express.json());
server.use(express.urlencoded({ extended: false }));
//Entrada al HOME
server.use("/", indexRouter);
//Entrada en rutas de character
server.use("/", characterRouter);
//Enter to planets section
server.use("/", planetsRouter);

//Control de errores
server.use((error, req, res) => {
    const status = error.status || 500;
    const message = error.message || "Unexpected error!";
});

server.listen(PORT, () => {
    console.log(`Server working in http://localhost:${PORT}`);
});