const mongoose = require("mongoose");

const CharactersSW = require("../models/CharacterSW");
const { DB_URL, CONFIG_DB } = require("../config/db")

const charactersSWArray = [{
        name: "Luke Skywalker",
        power: 89,
        side: "Ligth",
        type: "Human",
        job: "Jedi",
    },
    {
        name: "Darth Vader",
        power: 98,
        side: "Dark",
        type: "Cyborg",
        job: "Sith",
    },
    {
        name: "Obi Wan-Kenobi",
        power: 91,
        side: "Indiferent",
        type: "Human",
        job: "Jedi",
    },
    {
        name: "Bobba Fett",
        power: 79,
        side: "",
        type: "Human",
        job: "Bounty hunter",
    },
    {
        name: "Yoda",
        power: 99,
        side: "Ligth",
        type: "NaN",
        job: "Jedi",
    },
    {
        name: "Grievous",
        power: 92,
        side: "dark",
        type: "Cyborg",
        job: "Soldier",
    },
    {
        name: "Palpatine",
        power: 100,
        side: "Dark",
        type: "Human",
        job: "Sith",
    },
    {
        name: "Qui Gon-Jin",
        power: 87,
        side: "Light",
        type: "Human",
        job: "Jedi"
    }
];

//Mapeo de los personajes onstanciados con el molde
const characterSWDocument = charactersSWArray.map(character => new CharactersSW(character));

//Requierimiento a la DB para inscribir los personajes instacnciados
mongoose.connect(DB_URL, CONFIG_DB)
    .then(async() => {
        console.log('Ejecutando seed de Star Wars...');

        const allCharacters = await CharactersSW.find();
        console.log(allCharacters)

        if (allCharacters.length) { // si no hay elementos, 0 de length -> 0 convierte a false.
            await CharactersSW.collection.drop();
            console.log('Colección Characters de Star Wars eliminada');
        }
    })
    .catch(error => console.log('Error buscando en la DB', error))
    .then(async() => {

        await CharactersSW.insertMany(characterSWDocument);
        console.log('Añadidos nuevos personajes de SW a DB');
    })
    .catch(error => console.log('Error añadiendo los nuevos personajes', error))

.finally(() => mongoose.disconnect());