const express = require("express");
const CharactersSW = require("../models/CharacterSW");

const router = express.Router();

//----------------------GET---------------------------------
// Muestras los characters en general
router.get("/characters", async(req, res, next) => {
    try {
        const character = await CharactersSW.find(); //Character SW es nuestro 
        return res.status(200).json(character)
    } catch {
        return next("No se ha podido acceder a la DB", error);
    }
});

//Busca los personajes por ID
router.get("/characters/:id", async(req, res, next) => {
    const id = req.params.id;
    // const{id}=req.params; Es lo mismo que arriba
    try {
        const characterById = await CharactersSW.findById(id);
        if (characterById) {
            return res.status(200).json(characterById);
        } else {
            return res.status(404).json('No character found in DB with thid ID')
        }

    } catch (error) {
        console.log("No se ha podido acceder a la DB", error);
        return next("No se ha podido acceder a la DB", error);

    }
});

//Buscar personajes por nombre
router.get("/characters/name/:name", async(req, res, next) => {

    try {
        const name = req.params.name;
        const characterByName = await CharactersSW.find({ name: { $regex: new RegExp("^" + name.toLowerCase(), "i") } }); //Hacerlo insensible a mayusculas y minusculas
        console.log("character====>", characterByName);
        if (characterByName) {
            return res.status(200).json(characterByName);
        } else {
            return res.status(404).json('No character found in DB with this NAME')
        }

    } catch (error) {
        console.log("No se ha podido acceder a la DB", error);
        res.status(503).json({ message: "No se ha podido acceder a la DB", data: error });

    }
});
//Busccar power mayor a un numero
router.get("/characters/power/:num", async(req, res, next) => {
    try {
        const num = req.params.num;
        const characterByPower = await CharactersSW.find({ power: { $gt: num } }); //Permite buscar cantidades mayores a...
        console.log("character====>", characterByPower);
        if (characterByPower && num <= 100) {
            return res.status(200).json(characterByPower);
        } else if (num > 100) {
            return res.status(404).json('The limit POWER is 100')
        } else {
            return res.status(404).json('No character found in DB with this POWER')
        }

    } catch (error) {
        console.log("No se ha podido acceder a la DB", error);
        return next("No se ha podido acceder a la DB", error);

    }
});

//Buscar Dark o Light side
router.get("/characters/side/:side", async(req, res, next) => {
    try {
        const side = req.params.side;
        const characterBySide = await CharactersSW.find({ side: side }); //Permite buscar cantidades mayores a...
        console.log("character====>", characterBySide);
        if (characterBySide && side == "Ligth") {
            //Para poder enviar un mensaje en el JSON juntos con los datos debemos de poner "message" para la string y "data" para la DB
            return res.status(200).json({ message: "Estos personaje pertenece a la ORDEN JEDI o esta relacionado con ella ==>", data: characterBySide });
        } else if (characterBySide && side == "Dark") {
            return res.status(200).json({ message: "Estos personaje pertenece al JODIDO IMPERIO GALACTICO ==>", data: characterBySide });
        } else {
            return res.status(404).json('O perteneces a un lado o al otro, en medio va a ser que no')
        }

    } catch (error) {
        console.log("No se ha podido acceder a la DB", error);
        return next("No se ha podido acceder a la DB", error);

    }
});

//---------------------POST------------------------------
//Create new characters
router.post("/characters/create", async(req, res, next) => {
    try {
        const { name, power, side, type, job } = req.body;
        const newCharacter = new CharactersSW({
            name,
            power,
            side,
            type,
            job
        });
        const createdCharacter = await newCharacter.save();
        console.log("we are in post created==>", createdCharacter);
        return res.status(200).json(createdCharacter);
    } catch (error) {
        return next("No se ha podido acceder a la DB (post created)", error);
    }
});

//-----------PUT------------------------------
//Actualizamos nueestros èrsonajes por ID
router.put("/characters/edit/:id", async(req, res, next) => {
    try {
        const { id } = req.params;

        console.log("id", id);
        //Muestra las modificaciones hechas en el Body
        console.log("body", req.body);

        const editCharacterSw = new CharactersSW(req.body);
        editCharacterSw._id = id;
        const characterUpdateSw = await CharactersSW.findByIdAndUpdate(id, editCharacterSw, { new: true });
        console.log("we are in put edit==>", characterUpdateSw);
        return res.status(200).json(characterUpdateSw)

    } catch {
        return next("No se ha podido acceder a la DB (put edit)", error);
    }
});
//-------------------DELETE-------------------------
//Eliminamos nuestros personajes por ID
router.delete("/characters/delete/:id", async(req, res, next) => {
    try {
        const { id, name } = req.params;
        await CharactersSW.findByIdAndDelete(id);
        console.log(`Personaje con ID:${id} ha sido eliminado`);
        return res.status(200).json(`Character Delete : ${id}= ${name}`)
    } catch {
        return next("No se ha podido acceder a la DB (delete)", error);
    }
})
module.exports = router;