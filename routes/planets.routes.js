const express = require("express");
const CharactersSW = require("../models/CharacterSW");
const PlanetsSW = require("../models/PlanetsSW")

const router = express.Router();

// ------------------------------GET----------------------

router.get("/planets", async(req, res, next) => {
    try {
        // indicamos en .populate("coleccion de referencia","claves que queremos que muestre")
        const planetsSw = await PlanetsSW.find().populate("characters", "name power");
        console.log("Show planets list with characters", planetsSw);
        return res.status(200).json(planetsSw);
    } catch {
        console.log("No access to DB Planets", error);
        return next("No access to DB Planets", error);
    }
});
// -----------------------------POST-----------------
// Create planets
router.post("/planets/create", async(req, res, next) => {
        try {
            const { name, squad } = req.body;
            const newPlanet = new PlanetsSW({
                name,
                squad,
                characters: []
            });
            const createPlanet = await newPlanet.save();
            console.log(`Planet created : ${createPlanet}`);
            return res.status(201).json(createPlanet);
        } catch {
            console.log("No se ha podido acceder para crear PLANET", error);
            return next("No se ha podido acceder crear PLANET", error);
        }
    })
    // --------------------------------PUT------------------
    //Add characters who lives in planets
router.put("/planets/add-character", async(req, res, next) => {
        try {
            const { planetsId } = req.body;
            const { characterId } = req.body;
            const addCharacters = await PlanetsSW.findByIdAndUpdate(
                planetsId, { $push: { characters: characterId } }, { new: true }
            );
            console.log(`Add the next information: ${addCharacters}`);
            return res.status(202).json(addCharacters)

        } catch {
            console.log("No access to add character", error);
            return next("No access to add character", error);
        }
    })
    // ---------------------------DELETE------------------
    //Delete planets
router.delete("/planets/delete/:id", async(req, res, next) => {
    try {
        const { id } = req.params;
        await PlanetsSW.findByIdAndDelete(id);
        console.log(`Planet with ID:${id} had been eliminate`);
        return res.status(200).json(`Planet Delete : ${id}`)
    } catch {
        return next("No se ha podido acceder a la DB (delete)", error);
    }
})
module.exports = router